package at.hol.bankomat;

import java.util.Scanner;

public class Main {
	static Scanner s1 = new Scanner(System.in);
	
	public static void main(String[] args) {
	    int input;
	    int balance = 0;
	    boolean running = true;
	    
	    // Enter username and press Enter
	    System.out.println("Selektieren Sie die gew�nschte Funktion");
	    System.out.println("	1. Einzahlen");
	    System.out.println("	2. Abheben");
	    System.out.println("	3. Kontostand");
	    System.out.println("	4. Beenden");
	    
	    while (running == true) {
		    input = getInput();   
		    
		    switch(input) {
		    case 1:
		    	// Einzahlen
			    System.out.println("Geben Sie den gew�nschten Betrag ein:");
			    input = getInput();  
			    balance += input;
			    System.out.println("Sie haben " + input + " Euro eingezahlt");
		    	break;
		    case 2:
		    	// Abheben
			    System.out.println("Geben Sie den gew�nschten Betrag ein:");
			    input = getInput();  
			    balance -= input;
			    System.out.println("Sie haben " + input + " Euro abgehoben");
		    	
		    	break;
		    case 3:
		    	// Kontostand
			    System.out.println("der Kontostand betr�gt: " + balance);
		    	
		    	break;
		    case 4:
		    	// Ende
			    System.out.println("bye bye");
		    	running = false;
		    	break;
		    default:
			    System.out.println("Bitte eine g�ltige Eingabe machen");
		    }
	    }
	}
	
	public static int getInput() {
	    try {
	        return Integer.parseInt(s1.nextLine());
	    } catch (NumberFormatException e) {

		    System.out.println("Bitte eine g�ltige Eingabe machen");
	        return getInput();
	    }
	}
	
	
	

}
