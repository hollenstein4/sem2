package at.hol.hangman;

import java.util.ArrayList;
import java.util.Random;

public class HangmanLogic {
	
	String[] words = new String[10];
	String pickedWord = "";
	int charactersUncovered = 0;
	ArrayList<Character> pickedCovered = new ArrayList<Character>();
	Random r1 = new Random();
	int lifesLeft = 0;
	
	public HangmanLogic() {
		words[0] = "Milchshake";
		words[1] = "Bodensee";
		words[2] = "Wasser";
		words[3] = "Klimaerwärmung";
		words[4] = "Ländle";
		words[5] = "Corona";
		words[6] = "Dornbirn";
		words[7] = "Lustenau";
		words[8] = "Schweiz";
		words[9] = "Bregenz";
		
	}
	
	public void startGame() {
		pickedWord = words[r1.nextInt(10)];
		pickedCovered = new ArrayList<Character>(); //empty this
		charactersUncovered = 0;
		lifesLeft = 10;
		for (int i = 0; i < pickedWord.length(); i++) {
			pickedCovered.add((char) 42); //*
		}
	}
	
	
	public boolean compareInput(char input) {
		boolean gotOne = false;
		for (int i = 0; i < pickedWord.length(); i++) {
			if (pickedWord.charAt(i) == input && pickedCovered.get(i) == (char) 42) {
				pickedCovered.set(i, input);
				gotOne = true;
				charactersUncovered++;
			}
			//check for capital letters as well:
			if (input >= 97) {
				if (pickedWord.charAt(i) == input-32 && pickedCovered.get(i) == (char) 42) {
					pickedCovered.set(i, (char) (input-32));
					gotOne = true;
					charactersUncovered++;
				}
			}
		}
		return gotOne;
	}
	

	public String getCoveredWord() {
		String output = "";
		for (int i = 0; i < pickedCovered.size(); i++) {
			output += pickedCovered.get(i);
		}
		return output;
	}
	
	public void minusLife() {
		lifesLeft--;
	}
	
	public int getLifes() {
		return lifesLeft;
	}
	
	public String getPickedWord() {
		return pickedWord;
	}
	
	public boolean checkForWin() {
		if (charactersUncovered >= pickedWord.length()) {
			return true;
		} else {
			return false;
		}
	}
	
}
