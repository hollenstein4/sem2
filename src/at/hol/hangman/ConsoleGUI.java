package at.hol.hangman;

import java.util.Scanner;

public class ConsoleGUI implements GUI {

	Scanner s1 = new Scanner(System.in);

	@Override
	public char getInput() {
		String input = s1.next();
		
		return input.charAt(0);
		
	}

	@Override
	public void showLifes(int lifesLeft) {
		String lifeBar = "";
		int totalLifes = 10;
		for (int i = 0; i < lifesLeft; i++) {lifeBar += "#";}
		for (int i = 0; i < totalLifes-lifesLeft; i++) {lifeBar += "*";}
		
		System.out.println("||" + lifeBar + "|| " + lifesLeft + " Lifes left");
	}

	@Override
	public void showCovered(String coveredWord) {
		System.out.println(coveredWord);
		
	}

	@Override
	public void win() {
		System.out.println("Contratulation!");
	}
	
	@Override
	public void lost(String pickedWord) {
		System.out.println("Game Over! The word was " + pickedWord);
	}
	

	@Override
	public void askForRestart() {
		System.out.println("Press enter to restart");
		s1.nextLine();
		s1.nextLine();
	}


	
	
	
}
