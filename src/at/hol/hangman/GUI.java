package at.hol.hangman;

public interface GUI {
	public char getInput();
	public void showLifes(int lifesLeft);
	public void showCovered(String coveredWord);
	public void win();
	public void lost(String pickedWord);
	public void askForRestart();
}
