package at.hol.hangman;

public class Main {

	public static void main(String[] args) {
		HangmanLogic h1 = new HangmanLogic();
		ConsoleGUI c1 = new ConsoleGUI();

		while(true) {
			
			h1.startGame();
			while(true) {
	
				c1.showCovered(h1.getCoveredWord());
				char input = c1.getInput();
				boolean Match = h1.compareInput(input);
				if (Match == false) {
					h1.minusLife();
					int lifes = h1.getLifes();
					c1.showLifes(lifes);
					if (lifes == 0) {
						c1.lost(h1.getPickedWord());
						break;
					}
				} else {
					if (h1.checkForWin()) {
						c1.win();
						break;
					}
				}
			}
			c1.askForRestart();
		}
	}

}
