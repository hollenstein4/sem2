package at.hol.viergewinnt;

public class GameLogic {

	private char[][] field;
	private int[] fieldheight;
	
	public GameLogic() {
	}

	public void startGame(int columns, int rows) {
		field = new char[columns][rows];
		fieldheight = new int[(columns)];
	}
	
	public boolean addSelection(int selected, boolean player) {
		
		try {
			if (player) {
				field[selected][fieldheight[selected]] = 79; //O
			} else {
				field[selected][fieldheight[selected]] = 88; //X
			}
			fieldheight[selected]++;
			return true;
		} catch (NumberFormatException e) {
		
		    System.out.println("unknown error");
		    return false;
		} catch (ArrayIndexOutOfBoundsException e) {
		
		    System.out.println("Well, that doesn't work");
		    return false;
		}
	}
	
	
	public boolean checkForWin(char[][] field, int lastSelected) {
		boolean won = false;

		char playerChar = field[lastSelected][fieldheight[lastSelected]-1];
		
		int currentX = lastSelected;
		int currentY = fieldheight[lastSelected]-1;
		
		//horizontal
		int connected = 1;
		try {
			for (int i = 1; field[currentX+i][currentY] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentX-i][currentY] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		if (connected >= 4) won = true;
		
		//vertical
		connected = 1;
		try {
			for (int i = 1; field[currentX][currentY+i] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentX][currentY-i] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		if (connected >= 4) won = true;
		
		//diagonally right-up
		connected = 1;
		try {
			for (int i = 1; field[currentX+i][currentY+i] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentX-i][currentY-i] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		if (connected >= 4) won = true;
				
		//diagonally left-up
		connected = 1;
		try {
			for (int i = 1; field[currentX-i][currentY+i] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		try {
			for (int i = 1; field[currentX+i][currentY-i] == playerChar; i++) {
				connected++;
			}
		} catch (ArrayIndexOutOfBoundsException e) {}
		if (connected >= 4) won = true;
					
		return won;
	}

	
	
	
	
	public char[][] getField() {
		return field;
	}

	public void setField(char[][] field) {
		this.field = field;
	}

	public int[] getFieldheight() {
		return fieldheight;
	}

	public void setFieldheight(int[] fieldheight) {
		this.fieldheight = fieldheight;
	}

	
}
