package at.hol.viergewinnt;

import java.util.Scanner;

public class ConsoleGUI implements GUI {

	Scanner s1 = new Scanner(System.in);
	
	
	public void showGameField(char[][] field) {
		String currentRow = " | ";
		for (int j = 0; j < field.length; j++) {
			currentRow += j + " | ";
		}
		System.out.println(currentRow);
		for (int i = (field[0].length-1); i >= 0 ; i--) {
			currentRow = " | ";
		
			for (int j = 0; j < field.length; j++) {
				currentRow += (field[j][i] + " | ");
			}
		System.out.println(currentRow);
		}
	}
	
	public int getSelection(boolean player) {
		int column = 0;
		
		if (player) {

		    System.out.println("Enter your choice Player 1");
		} else {

		    System.out.println("Enter your choice Player 2");
		}
		try {
			String input = s1.next();
			column = Integer.parseInt(input);
		} catch (NumberFormatException e) {
			System.out.println("Bitte g�ltige Eingabe machen!");
			getSelection(player);
		}
		return column;
	}
	
	
}
