package at.hol.viergewinnt;


public class Main {

	
	public static void main(String[] args) {
		
		boolean player1turn = true;
		int columnNumber = 8;
		int rowNumber = 8;
		
		ConsoleGUI c1 = new ConsoleGUI();
		GameLogic g1 = new GameLogic();
		g1.startGame(columnNumber, rowNumber);
		c1.showGameField(g1.getField());
		
		while (true) {
			int selected = c1.getSelection(player1turn);
			boolean success = g1.addSelection(selected, player1turn);
			
			if (success) {
				c1.showGameField(g1.getField());
				
				if (g1.checkForWin(g1.getField(), selected)) {
					if (player1turn) {
					    System.out.println("Winner: O");
					} else {
					    System.out.println("Winner: X");
					}
					break;
				}
				player1turn = !player1turn;
			}
			
		}
	}
	
	

}
