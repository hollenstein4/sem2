package at.hol.tictactoe;

public class Main {

	
	public static void main(String[] args) {
		boolean player1Turn = true;
		TicTacToe t1 = new TicTacToe();
		
		t1.showGameField();
		
		while (true) {
			String[] selected = t1.nextTurn(player1Turn);

			boolean successful = t1.addSelect(selected, player1Turn);
			if (successful) {
				t1.showGameField();
				if (t1.checkForWin()) {
					if (player1Turn) {
					    System.out.println("Winner: O");
					} else {
					    System.out.println("Winner: X");
					}
				}
				player1Turn = !player1Turn;
			}
		}
	}
	
	

}
