package at.hol.tictactoe;

import java.util.Scanner;

public class TicTacToe {
	private char[][] field = new char[3][3];
	Scanner s1 = new Scanner(System.in);
	
	
	public void showGameField() {
		System.out.println(" | " + field[0][0] + " | " + field[0][1] + " | " + field[0][2] + " | ");
		System.out.println(" | " + field[1][0] + " | " + field[1][1] + " | " + field[1][2] + " | ");
		System.out.println(" | " + field[2][0] + " | " + field[2][1] + " | " + field[2][2] + " | ");
	}
	
	public boolean addSelect(String[] data, boolean player) {
		 try {
			 int axis0 = extractFromString(data, 0);
			 int axis1 = extractFromString(data, 1);
		     
			 if (field[axis0][axis1] > 0) {
				    System.out.println("This Field is already taken");
				 return false;
			 }
			 if (player) {
				 field[axis0][axis1] = 79; //O
			 } else {
				 field[axis0][axis1] = 88; //X
			 }
			 return true;
		        
	    } catch (NumberFormatException e) {

		    System.out.println("Wrong format! Write for example 0,0");
	        return false;
	    } catch (ArrayIndexOutOfBoundsException e) {

		    System.out.println("Wrong format! Write for example 0,0");
	        return false;
	    }
		 
	}
	
	public int extractFromString(String[] data, int axis) {
        return Integer.parseInt(data[axis]);
	}
	
	public String[] nextTurn(boolean player1Turn) {
		if (player1Turn) {

		    System.out.println("Enter your choice Player 1");
		} else {

		    System.out.println("Enter your choice Player 2");
		}
		

		String input = s1.next();
		return input.split(",");
		
	}
	
	public boolean checkForWin() {
		boolean someoneWon = false;
		if (field[0][0] + field[0][1] + field[0][2] == 237) someoneWon = true;
		if (field[1][0] + field[1][1] + field[1][2] == 237) someoneWon = true;
		if (field[2][0] + field[2][1] + field[2][2] == 237) someoneWon = true;
		
		if (field[0][0] + field[1][0] + field[2][0] == 237) someoneWon = true;
		if (field[0][1] + field[1][1] + field[2][1] == 237) someoneWon = true;
		if (field[0][2] + field[1][2] + field[2][2] == 237) someoneWon = true;

		if (field[0][0] + field[1][1] + field[2][2] == 237) someoneWon = true;
		if (field[0][2] + field[1][1] + field[2][0] == 237) someoneWon = true;
		
		
		if (field[0][0] + field[0][1] + field[0][2] == 264) someoneWon = true;
		if (field[1][0] + field[1][1] + field[1][2] == 264) someoneWon = true;
		if (field[2][0] + field[2][1] + field[2][2] == 264) someoneWon = true;
		
		if (field[0][0] + field[1][0] + field[2][0] == 264) someoneWon = true;
		if (field[0][1] + field[1][1] + field[2][1] == 264) someoneWon = true;
		if (field[0][2] + field[1][2] + field[2][2] == 264) someoneWon = true;

		if (field[0][0] + field[1][1] + field[2][2] == 264) someoneWon = true;
		if (field[0][2] + field[1][1] + field[2][0] == 264) someoneWon = true;
		
		return someoneWon;
	}
}
