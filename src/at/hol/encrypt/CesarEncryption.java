package at.hol.encrypt;

import java.util.ArrayList;

public class CesarEncryption implements Encrypter {

	private int shifts;
	
	public CesarEncryption(int shifts) {
		this.shifts = shifts;
	}

	@Override
	public String encrypt(String data) {
		String encrypted = "";
		for (int i = 0; i < data.length(); i++) {
			
			char character = data.charAt(i);
			int ascii = (int) character;
			

			if (ascii < 65 || ascii > 90 && ascii < 97 || ascii > 122) { //when not between A-Z or a-z, then don't shift

				encrypted += character;
			} else {

				char shifted = (char) (ascii + shifts);
				
				
				if (shifted < 65 || shifted > 90 && shifted < 97 || shifted > 122) { //when not between A-Z or a-z, then correct
					
					if (shifted > 122) { // >z
						shifted -= 26;
					} else if (shifted < 65) { // <A
						shifted += 26;
					} else if (shifted < 97) { // <a
						shifted += 26;
					} else if (shifted > 90) { // >Z
						shifted -= 26;
					}
				}
				encrypted += shifted;
			}
			
		}
		return encrypted;
	}

	@Override
	public String decrypt(String data) {
		String decrypted = "";
		for (int i = 0; i < data.length(); i++) {
			
			char character = data.charAt(i);
			int ascii = (int) character;

			
			if (ascii < 65 || ascii > 90 && ascii < 97 || ascii > 122) { //when not between A-Z or a-z, then don't shift

				decrypted += character;
			} else {

				char shifted = (char) (ascii - shifts);
				
				
				if (shifted < 65 || shifted > 90 && shifted < 97 || shifted > 122) { //when not between A-Z or a-z, then correct
					
					if (shifted > 122) { // >z
						shifted -= 26;
					} else if (shifted < 65) { // <A
						shifted += 26;
					} else if (shifted < 97) { // <a
						shifted += 26;
					} else if (shifted > 90) { // >Z
						shifted -= 26;
					}
				}
				decrypted += shifted;
			}
		}
		
		
		return decrypted;
	}

}
