package at.hol.encrypt;

public interface Encrypter {
	public String encrypt(String data);
	public String decrypt(String data);
	
}
